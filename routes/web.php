<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/users', 'UsersController@index')->name('users.index');
Route::get('/users/datatables', 'UsersController@datatables')->name('users.datatables');


Route::get('/cities/datatables', 'CitiesController@datatables')->name('cities.datatables');
Route::get('/cities', 'CitiesController@index')->name('cities.index');
Route::get('/cities/create', 'CitiesController@create')->name('cities.create');
Route::post('/cities', 'CitiesController@store')->name('cities.store');
Route::get('/cities/{id}', 'CitiesController@show')->name('cities.show');
Route::get('/cities/{id}/edit', 'CitiesController@edit')->name('cities.edit');
Route::put('/cities/{id}', 'CitiesController@update')->name('cities.update');
Route::delete('/cities/{id}', 'CitiesController@destroy')->name('cities.destroy');


Route::get('/clients/search', 'ClientsController@search')->name('clients.search');
Route::get('/clients/datatables', 'ClientsController@datatables')->name('clients.datatables');
Route::get('/clients', 'ClientsController@index')->name('clients.index');
Route::get('/clients/create', 'ClientsController@create')->name('clients.create');
Route::post('/clients', 'ClientsController@store')->name('clients.store');
Route::get('/clients/{id}', 'ClientsController@show')->name('clients.show');
Route::get('/clients/{id}/edit', 'ClientsController@edit')->name('clients.edit');
Route::put('/clients/{id}', 'ClientsController@update')->name('clients.update');
Route::delete('/clients/{id}', 'ClientsController@destroy')->name('clients.destroy');