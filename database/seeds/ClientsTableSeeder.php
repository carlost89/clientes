<?php

use App\City;
use App\Client;
use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->delete();

        $iterations = 30;

        $cities = City::take($iterations)->get();

        $this->command->getOutput()->progressStart($iterations);

        for ($i = 0; $i < $iterations; $i++) { 
            $client = new Client;
            $client->city_id = isset($cities[$i]) ? $cities[$i]->id : null;
            $client->cod = "cli_{$i}";
            $client->name = "Client {$i}";
            $client->save();

            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
    }
}
