<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $iterations = 30;

        $this->command->getOutput()->progressStart($iterations);

        for ($i = 0; $i < $iterations; $i++) { 
            $user = new User;
            $user->name = "User {$i}";
            $user->email = "user{$i}@mail.com";
            $user->password = bcrypt('123456');
            $user->save();

            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
    }
}
