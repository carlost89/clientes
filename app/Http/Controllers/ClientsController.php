<?php

namespace App\Http\Controllers;

use App\City;
use App\Client;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ClientsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('clients.index');
    }

    public function show(Request $request, $id)
    {
        $client = Client::findOrFail($id);

        return view('clients.show', ['client' => $client]);
    }

    public function edit(Request $request, $id)
    {
        $client = Client::findOrFail($id);

        $cities = City::get();

        return view('clients.edit', ['client' => $client, 'cities' => $cities]);
    }

    public function create()
    {
        $cities = City::get();

        return view('clients.create', ['cities' => $cities]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'city_id'   => 'required|exists:cities,id',
            'client_cod'       => 'required',
            'client_name'      => 'required',
        ]);

        $client = new Client;

        $client->city_id = $request->city_id;
        $client->cod = $request->client_cod;
        $client->name = $request->client_name;
        $client->save();

        return view('clients.index');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'city_id'   => 'required|exists:cities,id',
            'client_cod'       => 'required',
            'client_name'      => 'required',
        ]);

        $client = Client::findOrFail($id);

        $client->city_id = $request->city_id;
        $client->cod = $request->client_cod;
        $client->name = $request->client_name;
        $client->save();

        return view('clients.index');
    }

    public function destroy(Request $request, $id)
    {
        $client = Client::findOrFail($id);
        $client->delete();

        return view('clients.index');
    }

    public function datatables(Request $request)
    {
        $query = Client::query();

        $columns = ['actions'];

        if ( isset($request->city_id) ) {
            $query->whereHas('city', function($q) use ($request) {
                $q->where('id', $request->city_id);
            });
        }

        $dataTable = DataTables::of($query)->rawColumns($columns);

        $dataTable->addColumn('city_cod', function($object) {
            return isset($object->city) ? $object->city->cod : null;
        });

        $dataTable->addColumn('city_name', function($object) {
            return isset($object->city) ? $object->city->name : null;
        });

        $dataTable->addColumn('actions', function($object) {

            $route_1 = route('clients.show', $object->id);
            $button_1 = "<a href='{$route_1}' class='btn btn-success btn-sm' title='Ver'><i class='fa fa-eye'></i></a>";

            $route_2 = route('clients.edit', $object->id);
            $button_2 = "<a href='{$route_2}' class='btn btn-info btn-sm' title='Editar'><i class='fa fa-pencil'></i></a>";

            $route_3 = route('clients.destroy', $object->id);
            $csrf_field = csrf_field();
            $method_field = method_field('DELETE');
            $button_3 = "<form method='POST' action='{$route_3}'>"
                        .$csrf_field
                        .$method_field
                        ."<button class='btn btn-danger btn-sm'><i class='fa fa-minus'></i></button>"
                        ."</form>";

            return "{$button_1} {$button_2} {$button_3}";
        });

        return $dataTable->make(true);
    }

    public function search(Request $request)
    {
        if(isset($request->q)) {

            $query = City::query();
    
            $collection = $query->where('name', 'LIKE', "%{$request->q}%")
                    ->orWhere('cod', 'LIKE', "%{$request->q}%")
                    ->get();

            foreach ($collection as $key => $value) {

                $fullname = "{$value->cod} - {$value->name}";

                $results[$key] = [
                    'id'    => isset($value) ? $value->id : null,
                    'text'  => isset($value) ? $fullname : null
                ];
            }
        }

        $data = [
            'results' => isset($results) ? $results : [],
        ];

        return response()->json($data, 200);
    }
}
