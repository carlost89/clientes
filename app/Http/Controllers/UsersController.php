<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('users.index');
    }

    public function datatables()
    {
        $query = User::query();

        $columns = [];

        $dataTable = DataTables::of($query)->rawColumns($columns);

        return $dataTable->make(true);
    }
}
