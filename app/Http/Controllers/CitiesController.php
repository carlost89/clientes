<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('cities.index');
    }

    public function show(Request $request, $id)
    {
        $city = City::findOrFail($id);

        return view('cities.show', ['city' => $city]);
    }

    public function edit(Request $request, $id)
    {
        $city = City::findOrFail($id);

        return view('cities.edit', ['city' => $city]);
    }

    public function create()
    {
        return view('cities.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'city_cod'       => 'required',
            'city_name'      => 'required',
        ]);

        $city = new City;

        $city->cod = $request->city_cod;
        $city->name = $request->city_name;
        $city->save();

        return view('cities.index');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'city_cod'  => 'required',
            'city_name' => 'required',
        ]);

        $city = City::findOrFail($id);

        $city->cod = $request->city_cod;
        $city->name = $request->city_name;
        $city->save();

        return view('cities.index');
    }

    public function destroy(Request $request, $id)
    {
        $city = City::findOrFail($id);
        $city->delete();

        return view('cities.index');
    }

    public function datatables()
    {
        $query = City::query();

        $columns = ['actions'];

        $dataTable = DataTables::of($query)->rawColumns($columns);

        $dataTable->addColumn('actions', function($object) {

            $route_1 = route('cities.show', $object->id);
            $button_1 = "<a href='{$route_1}' class='btn btn-success btn-sm' title='Ver'><i class='fa fa-eye'></i></a>";

            $route_2 = route('cities.edit', $object->id);
            $button_2 = "<a href='{$route_2}' class='btn btn-info btn-sm' title='Editar'><i class='fa fa-pencil'></i></a>";

            $route_3 = route('cities.destroy', $object->id);
            $csrf_field = csrf_field();
            $method_field = method_field('DELETE');
            $button_3 = "<form method='POST' action='{$route_3}'>"
                        .$csrf_field
                        .$method_field
                        ."<button class='btn btn-danger btn-sm'><i class='fa fa-minus'></i></button>"
                        ."</form>";

            return "{$button_1} {$button_2} {$button_3}";
        });

        return $dataTable->make(true);
    }
}
