<?php

namespace App;

use App\City;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $connection = 'mysql';

    protected $table = 'clients';

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
}
