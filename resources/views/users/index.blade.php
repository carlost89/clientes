@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Users</h1>
            <br>
            <div class="table-responsive">
                <table class="table" id="dataTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>User Name</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var dataTable = $('#dataTable').DataTable({
                order: [0, 'desc'],
                autoWidth: false,
                processing: false,
                responsive: true,
                serverSide: true,
                searching: true,
                "ajax": {
                    url: '{{ route('users.datatables') }}',
                    data: function (d) {
                        d.test  = $('#test').val();
                    }
                },
                columns: [
                    {data: "id", name: 'id', searchable: true, orderable: true},
                    {data: "name", name: 'name', searchable: true, orderable: true},
                    {data: "email", name: 'email', searchable: true, orderable: true},

                ],
                "columnDefs": [
                    {"width": "5%", "targets":0, "className": "text-center text-bold"},
                    {"width": "5%", "targets":1, "className": "text-center text-bold"},
                    {"width": "5%", "targets":2, "className": "text-center text-bold"},
                ],
                "language": {          
                    "processing": "Generando",
                },
                "drawCallback": function( settings ) {
                    $('body').loading('stop');
                }
            });
    } );
</script>

@endsection
