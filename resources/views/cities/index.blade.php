@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="col-md-6 col-xs-6">
                <h1>Cities</h1>
                <br>
                <a href="{{ route('cities.create') }}" class="btn btn-success">Add New</a>
            </div>
            <br>
            <div class="table-responsive">
                <table class="table" id="dataTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Cod</th>
                            <th>City Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var dataTable = $('#dataTable').DataTable({
                order: [0, 'desc'],
                autoWidth: false,
                processing: false,
                responsive: true,
                serverSide: true,
                searching: true,
                "ajax": {
                    url: '{{ route('cities.datatables') }}',
                    data: function (d) {
                        d.test  = $('#test').val();
                    }
                },
                columns: [
                    {data: "id", name: 'id', searchable: true, orderable: true},
                    {data: "cod", name: 'cod', searchable: true, orderable: true},
                    {data: "name", name: 'name', searchable: true, orderable: true},
                    {data: "actions", name: null, searchable: false, orderable: false},
                ],
                "columnDefs": [
                    {"width": "5%", "targets":0, "className": "text-center text-bold"},
                    {"width": "5%", "targets":1, "className": "text-center text-bold"},
                    {"width": "5%", "targets":2, "className": "text-center text-bold"},
                    {"width": "5%", "targets":3, "className": "text-center text-bold"},
                ],
                "language": {          
                    "processing": "Generando",
                },
                "drawCallback": function( settings ) {
                    $('body').loading('stop');
                }
            });
    } );
</script>

@endsection
