@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="col-md-6 col-xs-6">
            <h1>Edit City {{ $city->id }}</h1>
            </div>
            <br>
            <div class="table-responsive">
                <table class="table" id="dataTable">
                    <thead>
                        <tr>
                            <th>City Cod</th>
                            <th>City Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <form action="{{ route('cities.update', $city->id) }}" method="POST">
                            
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}

                            <td>
                                <input type="text" class="form-control" name="city_cod" value="{{ $city->cod }}" required>
                            </td>
                            <td>
                                <input type="text" class="form-control" name="city_name" value="{{ $city->name }}" required>
                            </td>
                            <td>
                                <input type="submit" class="btn btn-success" value="Update">
                            </td>
                        </form>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection
