@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="col-md-6 col-xs-6">
            <h1>View City {{ $city->id }}</h1>
            </div>
            <br>
            <div class="table-responsive">
                <table class="table" id="dataTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>City Cod</th>
                            <th>City Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <td>{{ $city->id }}</td>
                        <td>{{ $city->cod }}</td>
                        <td>{{ $city->name }}</td>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection
