@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="col-md-6 col-xs-6">
            <h1>Create City</h1>
            </div>
            <br>
            <div class="table-responsive">
                <table class="table" id="dataTable">
                    <thead>
                        <tr>
                            <th>City Cod</th>
                            <th>City Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <form action="{{ route('cities.store') }}" method="POST">
                            
                            {{ csrf_field() }}

                            <td>
                                <input type="text" class="form-control" name="city_cod" required>
                            </td>
                            <td>
                                <input type="text" class="form-control" name="city_name" required>
                            </td>
                            <td>
                                <input type="submit" class="btn btn-success" value="Save">
                            </td>
                        </form>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection
