@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="col-md-6 col-xs-6">
            <h1>View Client {{ $client->id }}</h1>
            </div>
            <br>
            <div class="table-responsive">
                <table class="table" id="dataTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>City ID</th>
                            <th>City Cod</th>
                            <th>City Name</th>
                            <th>Client Cod</th>
                            <th>Client Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <td>{{ $client->id }}</td>
                        <td>{{ $client->city_id }}</td>
                        <td>{{ isset($client->city) ? $client->city->cod : null }}</td>
                        <td>{{ isset($client->city) ? $client->city->name : null }}</td>
                        <td>{{ $client->cod }}</td>
                        <td>{{ $client->name }}</td>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection
