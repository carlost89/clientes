@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="col-md-6 col-xs-6">
            <h1>Edit Client {{ $client->id }}</h1>
            </div>
            <br>
            <div class="table-responsive">
                <table class="table" id="dataTable">
                    <thead>
                        <tr>
                            <th>City</th>
                            <th>Client Cod</th>
                            <th>Client Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <form action="{{ route('clients.update', $client->id) }}" method="POST">
                            
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}

                            <td>
                                <select name="city_id" id="city_id" class="form-control" required>
                                    <option value="">Select a option</option>
                                    @foreach ($cities as $city)
                                        <option value="{{ $city->id }}" @if($client->city_id === $city->id) selected @endif>{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <input type="text" class="form-control" name="client_cod" value="{{ $client->cod }}" required>
                            </td>
                            <td>
                                <input type="text" class="form-control" name="client_name" value="{{ $client->name }}" required>
                            </td>
                            <td>
                                <input type="submit" class="btn btn-success" value="Update">
                            </td>
                        </form>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection
