@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="col-md-6 col-xs-6">
                <h1>Clients</h1>
                <br>
                <a href="{{ route('clients.create') }}" class="btn btn-success">Add New</a>
            </div>
            <br>
            <div class="col-md-6 col-xs-6">
                <label class="control-label"><b>Filter by City</b></label>
                <select class="form-control search change" id="city_id" name="city_id"></select>
            </div>
            <br>
            <div class="table-responsive">
                <table class="table" id="dataTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>City ID</th>
                            <th>City Cod</th>
                            <th>City Name</th>
                            <th>Client Cod</th>
                            <th>Client Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var dataTable = $('#dataTable').DataTable({
                order: [0, 'desc'],
                autoWidth: false,
                processing: false,
                responsive: true,
                serverSide: true,
                searching: true,
                "ajax": {
                    url: '{{ route('clients.datatables') }}',
                    data: function (d) {
                        d.city_id  = $('#city_id').val();
                    }
                },
                columns: [
                    {data: "id", name: 'id', searchable: true, orderable: true},
                    {data: "city_id", name: 'city_id', searchable: true, orderable: true},
                    {data: "city_cod", name: null, searchable: false, orderable: false},
                    {data: "city_name", name: null, searchable: false, orderable: false},
                    {data: "cod", name: 'cod', searchable: true, orderable: true},
                    {data: "name", name: 'name', searchable: true, orderable: true},
                    {data: "actions", name: null, searchable: false, orderable: false},

                ],
                "columnDefs": [
                    {"width": "5%", "targets":0, "className": "text-center text-bold"},
                    {"width": "5%", "targets":1, "className": "text-center text-bold"},
                    {"width": "5%", "targets":2, "className": "text-center text-bold"},
                    {"width": "5%", "targets":3, "className": "text-center text-bold"},
                    {"width": "5%", "targets":4, "className": "text-center text-bold"},
                    {"width": "5%", "targets":5, "className": "text-center text-bold"},
                    {"width": "5%", "targets":6, "className": "text-center text-bold"},
                ],
                "language": {          
                    "processing": "Searching",
                },
                "drawCallback": function( settings ) {
                    $('body').loading('stop');
                }
            });
    
        $(document).on('change', '.change', function (e) {
            e.preventDefault();
            $('body').loading({message: 'Searching'});
            dataTable.draw();
        });
    
    } );
</script>

<script>

    $('.search').select2({
        tags: false,
        placeholder: "Select an option",
        allowClear: true,
        width: '100%',
        language: 'es',
        ajax: {
            url: '{{ route("clients.search") }}',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                q: params.term, // search term
                page: params.page,
                };
            },
        }
    });

</script>

@endsection
