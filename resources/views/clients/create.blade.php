@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="col-md-6 col-xs-6">
            <h1>Create Client</h1>
            </div>
            <br>
            <div class="table-responsive">
                <table class="table" id="dataTable">
                    <thead>
                        <tr>
                            <th>City</th>
                            <th>Client Cod</th>
                            <th>Client Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <form action="{{ route('clients.store') }}" method="POST">
                            
                            {{ csrf_field() }}

                            <td>
                                <select name="city_id" id="city_id" class="form-control" required>
                                    <option value="" selected>Select a option</option>
                                    @foreach ($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <input type="text" class="form-control" name="client_cod" required>
                            </td>
                            <td>
                                <input type="text" class="form-control" name="client_name" required>
                            </td>
                            <td>
                                <input type="submit" class="btn btn-success" value="Save">
                            </td>
                        </form>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection
